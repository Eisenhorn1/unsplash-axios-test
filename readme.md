**Задача**

Создать Single Page Application, взаимодействующее с сервисом unsplash.com посредством public API.

[unsplash.com](https://unsplash.com/documentation)

### Установка
`npm install - установка модулей`

`npm run start - запуск приложения`

### Пояснения
При решении задания была использована JS-библиотека Axios, построенная на промисах http-клиента.