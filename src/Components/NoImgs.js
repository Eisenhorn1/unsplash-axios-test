import React from 'react';

const NoImgs = props => (
  <li className='no-imgs'>
    <h3>Изображений не найдено(</h3>
  </li>
);

export default NoImgs;