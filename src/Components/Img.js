import React from 'react';

const Img = props =>
	<li className="b-img-item">
		<div className="b-img-wrap">
			<img className="b-img" src={props.url} alt={props.desc} />
		</div>
		<div className="b-img-info">
			<p><b>Фото: </b>{props.name}</p>
			<p><b>Описание:</b> {props.desc}</p>
			<p><b>Дата:</b> {props.date}</p>
			<p>
				<a href={props.link} target="_blank">Ссылка</a>
			</p>
		</div>
	</li>;

export default Img;
