import React from 'react';
import Img from './Img';
import NoImgs from './NoImgs';

const ImgList = props => {
	const results = props.data;
	let imgs;
	if (results.length > 0) {
		imgs = results.map(img =>
			<Img
				url={img.urls.thumb}
				user={img.user.links.html}
				name={img.user.name}
				link={img.links.html}
				desc={img.description}
				date={img.created_at}
				key={img.id}
			/>
		);
		console.log(results);
	} else {
		imgs = <NoImgs />;
	}
	return (
		<div>
			<ul className="b-img-list">
				{imgs}
			</ul>
		</div>
	);
};

export default ImgList;
