import React, { Component } from 'react';
import axios from 'axios';

import './App.css';
import ImgList from './Components/ImgList';

export default class App extends Component {
	constructor() {
		super();
		this.state = {
			query: 'code',
			imgs: [],
			loadingState: true
		};
	}

	componentDidMount() {
		this.getData();
	}

	getData = (query = this.state.query) => {
		axios
			.get(
				`https://api.unsplash.com/search/photos/?page=1&per_page=10&query=${query}&client_id=c9238df0fcc0782bd17dd01a588975aa040efc4039484e2b47b425453109004e`
			)
			.then(data => {
				this.setState({ imgs: data.data.results, loadingState: false });
			})
			.catch(err => {
				console.log('Ошибка: ', err);
			});
	};

	render() {
		return (
			<div>
				<div className="b-header">
					<h1 className="b-title">
						SPA взаимодействующее с сервисом unsplash.com посредством public API
					</h1>
				</div>
				<div className="b-content">
					<p className="b-list-name">
						Поиск по слову: {this.state.query}
					</p>
					{this.state.loadingState
						? <p>Загрузка</p>
						: <ImgList data={this.state.imgs} />}
				</div>
			</div>
		);
	}
}
